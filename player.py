from turtle import Turtle

STARTING_POSITION = (0, -220)
MOVE_DISTANCE = 30


class Player(Turtle):
    def __init__(self):
        super().__init__()
        self.shape("turtle")
        self.up()
        self.setheading(90)
        self.goHome()

    def goUp(self):
        x_position = self.xcor()
        y_position = self.ycor()
        if y_position < 220:
            self.goto(x_position, y_position + MOVE_DISTANCE)
            #print(self.position())

    def goDown(self):
        x_position = self.xcor()
        y_position = self.ycor()
        if y_position > -220:
            self.goto(x_position, y_position - MOVE_DISTANCE)

    def goLeft(self):
        x_position = self.xcor()
        y_position = self.ycor()
        if x_position > -220:
            self.goto(x_position - MOVE_DISTANCE, y_position)

    def goRight(self):
        x_position = self.xcor()
        y_position = self.ycor()
        if x_position < 220:
            self.goto(x_position + MOVE_DISTANCE, y_position)

    def goHome(self):
        self.goto(STARTING_POSITION)
        
