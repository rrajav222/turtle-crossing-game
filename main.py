from scoreboard import Scoreboard
from turtle import Screen
from player import Player
from obstacles import Obstacle
import time

def turtle_crossing_game():
    screen = Screen()
    screen.clear()
    screen.setup(width=500, height=500)
    screen.tracer(0)
    screen.listen()

    player = Player()
    obstacle = Obstacle()
    score = Scoreboard()

    screen.onkey(player.goUp, "Up")
    screen.onkey(player.goDown, "Down")
    screen.onkey(player.goLeft, "Left")
    screen.onkey(player.goRight, "Right")

    game_is_on = True

    while game_is_on:
        time.sleep(0.1)

        obstacle.add_obstacle()
        obstacle.move()

        for each_obstacle in obstacle.obstacles_list:
            if each_obstacle.ycor() == player.ycor() and each_obstacle.distance(player) < 33:
                #print(f"obs {each_obstacle.ycor()}")
                #print(f"player {player.ycor()}")
                game_is_on = False
                score.gameOver()

        if player.ycor() == 230:
            print(player.ycor())
            print("Win")
            player.goHome()
            obstacle.levelUp()
            score.updateLevel()
            #game_is_on = False
            # score.gameOver()

        screen.update()
    
    screen.onkey(turtle_crossing_game,"space")
    screen.exitonclick()

turtle_crossing_game()