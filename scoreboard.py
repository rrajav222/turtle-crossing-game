from turtle import Turtle

ALIGNMENT = "center"
FONT = ("Courier", 15, "bold")


class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.up()
        self.goto(-180, 210)
        self.level = 1
        self.updateScoreBoard()

    def updateScoreBoard(self):
        self.clear()
        self.write(f"Level : {self.level}", align=ALIGNMENT, font=FONT)

    def updateLevel(self):
        self.level += 1
        self.updateScoreBoard()

    def gameOver(self):
        #self.clear()
        self.goto(0,0)
        self.write("GAME OVER!", align=ALIGNMENT, font=FONT)
        self.goto(0,-20)
        self.write("Press [SPACE} to start again", align=ALIGNMENT, font=FONT)

