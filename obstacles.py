from turtle import Turtle
import random

OBSTABLES_COORDINATES = []
OBSTABLES_COLORS = ['red', 'blue', 'yellow', 'green', 'orange', 'purple']
STARTING_MOVE_DISTANCE = 10
MOVE_INCREMENT = 5

for i in range(-160, 200, 30):
    OBSTABLES_COORDINATES.append(i)

class Obstacle:
    def __init__(self):
        self.obstacles_list = []
        self.car_speed = STARTING_MOVE_DISTANCE
        self.no_random_chance = 3
        self.add_obstacle()

    def add_obstacle(self):
        random_chance = random.randint(1,self.no_random_chance)
        if random_chance == 1:
            obstacle = Turtle()
            obstacle.shape("square")
            obstacle.color(random.choice(OBSTABLES_COLORS))
            obstacle.up()
            obstacle.shapesize(stretch_wid=1, stretch_len=2)
            obstacle.goto(260, random.choice(OBSTABLES_COORDINATES))
            self.obstacles_list.append(obstacle)

    def move(self):
        for each_obstacle in self.obstacles_list:
            each_obstacle.setheading(180)
            each_obstacle.fd(self.car_speed)

    def levelUp(self):
        self.car_speed += MOVE_INCREMENT
        if self.no_random_chance > 1: 
            self.no_random_chance -= 1

